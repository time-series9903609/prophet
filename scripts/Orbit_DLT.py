import pandas as pd
import os 
import numpy as np
import fileUtils 
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt 
import orbit
from orbit.models import DLT 
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
# from orbit.diagnostics.plot import plot_predicted_data

if __name__ == "__main__":

    input_df = fileUtils.read_csv('input_data')
    print(input_df.shape)
    print(input_df.head(5))

    # scaler = StandardScaler()

    # # select numerical columns
    # num_cols = ['sales', 'transactions', 'onpromotion']
    # input_df[num_cols] = scaler.fit_transform(input_df[num_cols])
    print(input_df.head())
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    print(input_df.info())
    print(input_df.columns)
    # input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    print(input_df.columns)

    forecasts = {}

    for store_item in input_df['store_item_id'].unique():

        series_df = input_df[input_df['store_item_id']==store_item]
        print(series_df.columns)
        series_df = series_df.sort_values("date", ascending=True)
        # series_df = series_df.reset_index(drop=True, inplace=True)
        series_df = pd.DataFrame(series_df)
        print(type(series_df))
        print(series_df.columns)
        print(series_df.duplicated().any())
        print(series_df.isna().sum())
        # series_df.to_csv('Orbit_series.csv')
        # Assuming 'df' is your DataFrame and 'date_column' is the name of the date column
        series_df.drop_duplicates(subset=['date'], keep='first', inplace=True)
        train_df = series_df[(series_df['date'].dt.year >= 2013) & (series_df['date'].dt.year <= 2016)]
        # train_df = train_df.reset_index(drop=True, inplace=True)
        # train_df.to_csv('Orbit_train.csv')
        test_df = series_df[series_df['date'].dt.year == 2017]
        # test_df = test_df.reset_index(drop=True, inplace=True)
        # test_df.to_csv('Orbit_test.csv')
    # train_df = input_df[input_df['date'].dt.year.isin([2013, 2014, 2015, 2016])]
    # test_df = input_df[input_df['date'].dt.year == 2017]

        column_names = ['onpromotion','transactions']

        model = DLT(response_col = 'sales', 
                    date_col='date', 
                    seasonality=365,
                    global_trend_option='linear', 
                    damped_factor=0.4, 
                    regressor_col=column_names)

        # print(train_df.head(5))
        model.fit(df=train_df)

        forecast_df = model.predict(test_df, decompose=True)
        print(forecast_df.head(5))

        forecasts[store_item] = forecast_df

    # print(forecast_df.head())

    # plot_predicted_data(training_actual_df=input_df, 
    #                     predicted_df=forecast_df, 
    #                     date_col='date', 
    #                     actual_col='sales', 
    #                     pred_col='prediction',
    #                     prediction_percentiles=['0.05', '0.95'])
    # plt.show()

    # # Calculate metrics
    # lr_mse = mean_squared_error(test_df['y'].astype(float), forecast_df['yhat'])
    # lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast_df['yhat'])

    # # Apply the absolute value function to both y_eval and lr_predictions
    # y_eval_abs = abs(test_df['y'].astype(float))
    # lr_predictions_abs = abs(forecast_df['yhat'])

    # # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    # lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # # Create a DataFrame to store results for Linear Regression
    # results_lr = pd.DataFrame({'Model': ['Orbit DLT Model'],
    #                         'RMSLE': [lr_rmsle],
    #                         'RMSE': [np.sqrt(lr_mse)],
    #                         'MSE': [lr_mse],
    #                         'MAE': [lr_mae]}).round(2)

    # # Print the results_lr dataframe
    # print(results_lr)


