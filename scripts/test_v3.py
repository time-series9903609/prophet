import pandas as pd 
import numpy as np 
# from download import download
import fileUtils
import matplotlib.pyplot as plt
import seaborn as sns
import os



if __name__=="__main__":
    
   train_df = fileUtils.read_csv('train')
   print(train_df.shape)
   print(train_df.columns)
   print(train_df.isna().any())

   stores_df = fileUtils.read_csv('stores')
   print(stores_df.shape)
   print(stores_df.columns)
   print(stores_df.isna().any())

   train_store_df = pd.merge(train_df, stores_df, on='store_nbr', how='inner')
   print(train_store_df.shape)
   print(train_store_df.columns)
   print(train_store_df.isna().any())

   oil_df = fileUtils.read_csv('oil')
   print(oil_df.shape)
   print(oil_df.columns)
   print(oil_df.isna().any())
   print(oil_df.isna().sum())

   transactions_df = fileUtils.read_csv('transactions')
   print(transactions_df.shape)
   print(transactions_df.columns)
   print(transactions_df.isna().any())


   train_store_transact_df = pd.merge(train_store_df,transactions_df, on=['date','store_nbr'], how='left')
   print(train_store_transact_df.shape)
   print(train_store_transact_df.columns)
   print(train_store_transact_df.isna().any())
   print(train_store_transact_df.isna().sum())

   median_transactions = train_store_transact_df['transactions'].median()
   train_store_transact_df.fillna({'transactions':median_transactions}, inplace=True)
   print(train_store_transact_df.isna().sum())
   print(train_store_transact_df.shape)

   holiday_df = fileUtils.read_csv('holidays_events')
   print(holiday_df.shape)
   print(holiday_df.columns)
   print(holiday_df.isna().any())
   print(holiday_df.isna().sum())

   final_df = pd.merge(train_store_transact_df, holiday_df, on='date', how='left')
   print(final_df.shape)
   print(final_df.columns)
   print(final_df.isna().sum())

   final_df = final_df.drop(['type_y','locale','locale_name','description','transferred'], axis=1)
   print(final_df.shape)
   print(final_df.isna().sum())
   print(final_df.columns)
   final_df.rename(columns={'type_x':'store_type'}, inplace=True)
   print(final_df.columns)

   # Adding missing dates
   # Check the completeness of the train dataset
   min_date = final_df['date'].min()
   max_date = final_df['date'].max()
   expected_dates = pd.date_range(start=min_date, end=max_date)

   missing_dates = expected_dates[~expected_dates.isin(final_df['date'])]

   if len(missing_dates) == 0:
      print("The train dataset is complete. It includes all the required dates.")
   else:
      print("The train dataset is incomplete. The following dates are missing:")
      print(missing_dates)
   
   # Create a DataFrame with the missing dates, using the 'date' column
   missing_data = pd.DataFrame({'date': missing_dates})

   # Concatenate the original train dataset and the missing data DataFrame
   # ignore_index=True ensures a new index is assigned to the resulting DataFrame
   final_df = pd.concat([final_df, missing_data], ignore_index=True)

   # Sort the DataFrame based on the 'date' column in ascending order
   final_df['date'] = pd.to_datetime(final_df['date'])
   final_df.sort_values('date', inplace=True)

   missing_dates = expected_dates[~expected_dates.isin(final_df['date'])]

   if len(missing_dates) == 0:
      print("The train dataset is complete. It includes all the required dates.")
   else:
      print("The train dataset is incomplete. The following dates are missing:")
      print(missing_dates)

   print(final_df.isna().sum())
   final_df.fillna(method='bfill', inplace=True)
   print(final_df.isna().sum())

   # cleaning_families_list = ['HOME CARE', 'BABY CARE', 'PERSONAL CARE']
   cleaning_families_list = ['PERSONAL CARE']
   final_df = final_df[(final_df['family'].isin(cleaning_families_list)) & (final_df['store_nbr']==1)]
   print(final_df.shape)
   print(final_df.family.unique())
   print(len(final_df.family.unique()))

   # Remove special characters and convert to lowercase
   final_df['family'] = final_df['family'].str.replace('/', '_').str.replace(' ', '_').str.lower()
   final_df['city'] = final_df['city'].str.replace('/', '_').str.replace(' ', '_').str.lower()
   final_df = final_df.drop('state', axis=1)

   final_df['store_nbr'] = final_df['store_nbr'].astype(int)
   final_df['store_nbr'] = final_df['store_nbr'].astype(str)
   final_df['family'] = final_df['family'].astype(str)
   final_df["store_item_id"] = final_df.apply(lambda x: f"{x['store_nbr']}_{x['family']}", axis=1)
   final_df = final_df.drop(['store_nbr','family','city','store_type','cluster'], axis=1)
   print(final_df.columns)

   final_df = final_df.groupby(["date","store_item_id"]).agg({'sales':'sum','onpromotion':'sum','transactions':'sum'})
   final_df = final_df.reset_index()
   final_df = final_df.sort_values("date", ascending=True)
   print(final_df.head())

   # # total_sales_per_date = store_item_sales_per_date.groupby(["date"])["sales"].aggregate("sum")
   # # total_sales_per_date.plot()
   # # plt.show()

   store_items = final_df['store_item_id'].unique()
   print("Number of items ",len(store_items))

   complete_dfs = []

   for product in store_items:
      product_df = final_df[final_df['store_item_id']==product]
      min_date = product_df['date'].min()
      max_date = product_df['date'].max()
      expected_dates = pd.date_range(start=min_date, end=max_date)

      missing_dates = expected_dates[~expected_dates.isin(product_df['date'])]

      if len(missing_dates) == 0:
         print("The train dataset is complete. It includes all the required dates.")
      else:
         print("The train dataset is incomplete. The following dates are missing:")
         print(missing_dates)
      
      # Create a DataFrame with the missing dates, using the 'date' column
      missing_data = pd.DataFrame({'date': missing_dates})

      # Concatenate the original train dataset and the missing data DataFrame
      # ignore_index=True ensures a new index is assigned to the resulting DataFrame
      product_df = pd.concat([product_df, missing_data], ignore_index=True)

      # Sort the DataFrame based on the 'date' column in ascending order
      product_df['date'] = pd.to_datetime(product_df['date'])
      product_df.sort_values('date', inplace=True)

      missing_dates = expected_dates[~expected_dates.isin(product_df['date'])]
      print(product_df.tail())
      product_df.fillna({'sales': 0}, inplace=True)
      product_df.fillna({'store_item_id': product}, inplace=True)
      product_df.fillna({'onpromotion': 0}, inplace=True)
      product_df.fillna({'transactions': 0}, inplace=True)

      complete_dfs.append(product_df)
   
   input_df = pd.concat(complete_dfs)
   min_date = input_df['date'].min()
   max_date = input_df['date'].max()
   expected_dates = pd.date_range(start=min_date, end=max_date)

   missing_dates = expected_dates[~expected_dates.isin(input_df['date'])]

   if len(missing_dates) == 0:
      print("The train dataset is complete. It includes all the required dates.")
   else:
      print("The train dataset is incomplete. The following dates are missing:")
      print(missing_dates) 

   # input_df.drop(columns=['id'], axis=1, inplace=True)
   print(input_df.isna().sum())
   print(input_df.shape)
   print(input_df.columns)
#    print(input_df.family.unique())

   # save final dataset
   current_dir = os.path.dirname(os.path.abspath(__file__))
   data_folder = os.path.join(current_dir, '../data')
   filename_with_extension = 'input_data_item_store' + '.csv'
   file_path = os.path.join(data_folder, filename_with_extension)
   input_df.to_csv(file_path)
   
   













