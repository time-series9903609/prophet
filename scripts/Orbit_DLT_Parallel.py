import pandas as pd
import os
from sklearn.preprocessing import StandardScaler
import orbit
from orbit.models import DLT 
from multiprocessing import Pool

def fit_model(train_data):

    train_data = train_data.drop_duplicates(subset=['date'], keep='first')
    train_df = train_data[(train_data['date'].dt.year >= 2013) & (train_data['date'].dt.year <= 2016)]
    
    model = DLT(
        response_col='sales',
        date_col='date',
        seasonality=365,
        global_trend_option='linear',
        damped_factor=0.4,
        regressor_col=['onpromotion', 'transactions']
    )
    model.fit(train_df)
    
    return model

def predict_forecast(args):
    model, test_data = args
    forecast = model.predict(test_data)
    return forecast

if __name__ == "__main__":
    input_df = pd.read_csv('input_data')  # Assuming 'input_data' is the file containing input data
    
    scaler = StandardScaler()
    num_cols = ['sales', 'transactions', 'onpromotion']
    input_df[num_cols] = scaler.fit_transform(input_df[num_cols])
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    
    data_subsets = []

    for store_item in input_df['store_item_id'].unique():
        series_df = input_df[input_df['store_item_id'] == store_item].copy()
        train_data = series_df.copy()
        test_data = series_df[series_df['date'].dt.year == 2017].copy()
        data_subsets.append((train_data, test_data))
    
    with Pool(processes=4) as pool:  # Adjust the number of processes based on the available CPU cores
        models = pool.map(fit_model, [subset[0] for subset in data_subsets])
        forecasts = pool.map(predict_forecast, zip(models, [subset[1] for subset in data_subsets]))
        
    print(forecasts.columns)
    print(forecasts.head(10))
