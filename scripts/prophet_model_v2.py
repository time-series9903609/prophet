import pandas as pd
import os 
import numpy as np
import fileUtils 
from sklearn.preprocessing import StandardScaler
from prophet import Prophet
import matplotlib.pyplot as plt  
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from multiprocessing import Pool, cpu_count
from typing import List, Any
from datetime import datetime

def fit_model(train_data):
    try:        
        model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    # holidays=holiday_df,
                    # holidays_mode='additive',
                    seasonality_mode='additive')
        model.add_regressor('onpromotion')
        model.add_regressor('transactions')
        model.fit(train_data)
        return model
    except Exception as e:
        print(f"Error fitting model: {e}")
        return None

def predict_forecast(model, test_data):
    try:
        forecast = model.predict(test_data)
        return forecast
    except Exception as e:
        print(f"Error predicting forecast: {e}")
        return None

def fit_and_predict(df:pd.DataFrame):
    train_data = df.loc[df['test_indicator'] == 0]
    test_data = df.loc[df['test_indicator'] == 1]
    model = fit_model(train_data)
    if model:
        forecast = predict_forecast(model, test_data)
        return forecast
    else:
        return None

if __name__ == "__main__":

    input_df = fileUtils.read_csv('input_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    grouped_df = input_df.groupby(['store_item_id', 'ds']).agg({'y':'sum', 'transactions':'sum', 'onpromotion':'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    series = [grouped_df.loc[grouped_df.store_item_id == x] for x in grouped_df.store_item_id.unique()]

    # holiday_df = fileUtils.read_csv('prophet_holidays')
    # print(holiday_df.columns)
    # holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed',case=False)], axis=1)
    # print(holiday_df.columns)

    fit_predict_time = datetime.now()
    print("Fit and Predict Start time :", fit_predict_time)
    p = Pool(cpu_count())        
    forecast: List[Any] = list(p.imap(fit_and_predict, series))
    fit_predict_timef = datetime.now()
    print("Fit and Predict End time :", fit_predict_timef)  
    print("Total Fit and Predict time :", fit_predict_timef-fit_predict_time)

    forecast_dataframe = pd.concat(forecast)
    
    print(forecast_dataframe.columns)
    print(forecast_dataframe.head(10))

    # model = Prophet(interval_width = 0.95,
    #                 growth='linear',
    #                 yearly_seasonality=True,
    #                 changepoint_range=0.95,
    #                 holidays=holiday_df,
    #                 holidays_mode='additive',
    #                 seasonality_mode='additive')
    # model.add_regressor('onpromotion')
    # model.add_regressor('transactions')

    # model.fit(train_df)
    # forecast_df = model.predict(test_df)
    # print(forecast_df.head())
   
    # model.plot_components(forecast_df)
    # plt.show()

    # # Calculate metrics
    # lr_mse = mean_squared_error(test_df['y'].astype(float), forecast_df['yhat'])
    # lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast_df['yhat'])

    # # Apply the absolute value function to both y_eval and lr_predictions
    # y_eval_abs = abs(test_df['y'].astype(float))
    # lr_predictions_abs = abs(forecast_df['yhat'])

    # # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    # lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # # Create a DataFrame to store results for Linear Regression
    # results_lr = pd.DataFrame({'Model': ['Prophet Model'],
    #                         'RMSLE': [lr_rmsle],
    #                         'RMSE': [np.sqrt(lr_mse)],
    #                         'MSE': [lr_mse],
    #                         'MAE': [lr_mae]}).round(2)

    # # Print the results_lr dataframe
    # print(results_lr)


