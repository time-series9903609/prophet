import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
from multiprocessing import Pool, cpu_count
from typing import List, Any
import orbit
from orbit.models import KTR 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime  

def fit_model(train_data):
    # fitting_time = datetime.now()
    # print(" Fitting Start time ", fitting_time)
    try:
        column_names = ['onpromotion', 'transactions']
        
        model = KTR(response_col='sales', 
                    date_col='date', 
                    seasonality=365.25,
                    # global_trend_option='linear', 
                    # damped_factor=0.4, 
                    regressor_col=column_names)
        model.fit(train_data)
        # fitting_timef = datetime.now()
        # print(" Fitting End time ", fitting_timef)
        # print("Total fitting time : ", fitting_timef-fitting_time)
        return model
    except Exception as e:
        print(f"Error fitting model: {e}")
        return None
    

def predict_forecast(model, test_data):
    # predict_time = datetime.now()
    # print("Prediction start time : ", predict_time)
    try:
        forecast = model.predict(test_data)
        # predict_timef = datetime.now()
        # print("Prediction end time : ", predict_timef)
        # print("Total Prediction time : ", predict_timef-predict_time)
        return forecast
    except Exception as e:
        print(f"Error predicting forecast: {e}")
        return None
    

def fit_and_predict(df:pd.DataFrame):
    
    train_data = df.loc[df['test_indicator'] == 0]
    test_data = df.loc[df['test_indicator'] == 1]
    model = fit_model(train_data)
    if model:
        forecast = predict_forecast(model, test_data)
        return forecast
    else:
        return None

if __name__ == "__main__":
    # try:
    input_df = fileUtils.read_csv('input_data_item')
    print(input_df.shape)
    print(input_df.store_item_id.unique())

    # scaler = StandardScaler()
    # num_cols = ['sales', 'transactions', 'onpromotion']
    # input_df[num_cols] = scaler.fit_transform(input_df[num_cols])
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')

    grouped_df = input_df.groupby(['store_item_id', 'date']).agg({'sales':'sum', 'transactions':'sum', 'onpromotion':'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['date'].dt.year >= 2017, 'test_indicator'] = 1
    series = [grouped_df.loc[grouped_df.store_item_id == x] for x in grouped_df.store_item_id.unique()]
    # print(" First Element >>>>>>>>>>",series[0])

    p = Pool(cpu_count())  
    fit_predict_time = datetime.now()
    print("Fit and Predict start time : ", fit_predict_time)      
    forecast: List[Any] = list(p.imap(fit_and_predict, series))  
    fit_predict_timef = datetime.now()
    print("Fit and Predict end time : ", fit_predict_timef)
    print("Total Fit and Predict time : ", fit_predict_timef-fit_predict_time)
    forecast_dataframe = pd.concat(forecast)
    
    print(forecast_dataframe.columns)
    print(forecast_dataframe.head(10))


