import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
from multiprocessing import Pool, cpu_count
from typing import List, Any
import orbit
from orbit.models import DLT 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime 

def fit_model(train_data):
    try:
        column_names = ['onpromotion', 'transactions']
        
        model = DLT(response_col='sales', 
                    date_col='date', 
                    seasonality=365,
                    estimator='stan-mcmc',
                    global_trend_option='linear', 
                    damped_factor=0.4, 
                    regressor_col=column_names
                    # obs_sigma=0.000001
                    )
        model.fit(train_data)
        return model
    except Exception as e:
        print(f"Error fitting model: {e}")
        return None

def predict_forecast(model, test_data):
    try:
        forecast = model.predict(test_data)
        return forecast
    except Exception as e:
        print(f"Error predicting forecast: {e}")
        return None

def fit_and_predict(df:pd.DataFrame):
    train_data = df.loc[df['test_indicator'] == 0]
    test_data = df.loc[df['test_indicator'] == 1]
    model = fit_model(train_data)
    if model:
        forecast = predict_forecast(model, test_data)
        return forecast
    else:
        return None

if __name__ == "__main__":
    # try:
    input_df = fileUtils.read_csv('input_data_item')

    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')

    grouped_df = input_df.groupby(['store_item_id', 'date']).agg({'sales':'sum', 'transactions':'sum', 'onpromotion':'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['date'].dt.year >= 2017, 'test_indicator'] = 1
    series = [grouped_df.loc[grouped_df.store_item_id == x] for x in grouped_df.store_item_id.unique()]
    test_df = grouped_df.loc[grouped_df.test_indicator == 1]
    print("test data shape :", test_df.shape)
    print("test data Null values :", test_df.isna().sum())
    print("test data Top 5 rows :", test_df.head(5))
    print("test data Last 5 rows :", test_df.tail(5))
    test_df.to_csv('test_data_file.csv')


    fit_predict_time = datetime.now()
    print("Fit and Predict Start time :", fit_predict_time)
    p = Pool(cpu_count())        
    forecast: List[Any] = list(p.imap(fit_and_predict, series))
    fit_predict_timef = datetime.now()
    print("Fit and Predict End time :", fit_predict_timef)  
    print("Total Fit and Predict time :", fit_predict_timef-fit_predict_time)

    forecast_dataframe = pd.concat(forecast)

    print("forecast data shape :", forecast_dataframe.shape)
    print("forecast data Null values :", forecast_dataframe.isna().sum())
    print("forecast data Top 5 rows :", forecast_dataframe.head(5))
    print("forecast data Last 5 rows :", forecast_dataframe.tail(5))
    forecast_dataframe.to_csv('forecast_data_file.csv')
    
    print(forecast_dataframe.columns)
    print(forecast_dataframe.head(10))

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['sales'].astype(float), forecast_dataframe['prediction'])
    lr_mae = mean_absolute_error(test_df['sales'].astype(float), forecast_dataframe['prediction'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['sales'].astype(float))
    lr_predictions_abs = abs(forecast_dataframe['prediction'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Orbit Model 1 item X 54 stores'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)],
                            'MSE': [lr_mse],
                            'MAE': [lr_mae]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)


