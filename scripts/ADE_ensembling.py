import numpy as np
import pandas as pd
from prophet import Prophet
import orbit
from orbit.models import DLT
from sklearn.metrics import mean_squared_error, root_mean_squared_error
from sklearn.model_selection import train_test_split
import fileUtils

# Define base learners
def train_prophet(train_data):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    seasonality_mode='additive')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.fit(train_df)
    return model

def train_orbit(train_data):
    model = DLT(
        response_col='y',
        date_col='ds',
        seasonality=365,
        global_trend_option='linear',
        damped_factor=0.4,
        regressor_col=['onpromotion', 'transactions']
    )
    model.fit(train_df)
    return model

# Make predictions using base learners
def predict_prophet(models, data):
    predictions = np.zeros((len(data),))
    for model in models:
        forecast = model.predict(data)
        predictions += forecast['yhat'].values
    return predictions / len(models)

def predict_orbit(models, data):
    predictions = np.zeros((len(data),))
    for model in models:
        forecast = model.predict(data)
        predictions += forecast['prediction'].values
    return predictions / len(models)

# Make ensemble prediction using ADE
def ade_ensemble_predict(prophet_models, orbit_models, data):
    predictions = np.zeros((len(data),))
    prophet_weights = np.zeros((len(prophet_models),))
    orbit_weights = np.zeros((len(orbit_models),))
    for i, model in enumerate(prophet_models):
        forecast = model.predict(data)
        predictions += forecast['yhat'].values
        prophet_weights[i] = root_mean_squared_error(data['y'].values, forecast['yhat'].values, squared=False)
    for i, model in enumerate(orbit_models):
        forecast = model.predict(data)
        predictions += forecast['prediction'].values
        orbit_weights[i] = root_mean_squared_error(data['y'].values, forecast['prediction'].values, squared=False)
    # Normalize weights
    total_error = np.sum(prophet_weights) + np.sum(orbit_weights)
    prophet_weights /= total_error
    orbit_weights /= total_error
    # Weighted average of predictions
    predictions /= (len(prophet_models) + len(orbit_models))
    return predictions


if __name__ == "__main__":

    input_df = fileUtils.read_csv('input_data_item_store')
    print(input_df.shape)
    print(input_df.head(5))

    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    train_df = input_df[input_df['ds'].dt.year.isin([2013, 2014, 2015, 2016])]
    test_df = input_df[input_df['ds'].dt.year == 2017]

    # Train multiple base learners
    prophet_models = [train_prophet(train_df) for _ in range(5)]
    orbit_models = [train_orbit(train_df) for _ in range(5)]


    # Make ensemble prediction using ADE
    ensemble_prediction = ade_ensemble_predict(prophet_models, orbit_models, test_df)

    # Evaluate ensemble performance
    true_values = test_df['y'].values
    rmse_ensemble = root_mean_squared_error(true_values, ensemble_prediction, squared=False)
    print("RMSE of ADE ensemble:", rmse_ensemble)
