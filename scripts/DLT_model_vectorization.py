import pandas as pd
import numpy as np
from concurrent.futures import ThreadPoolExecutor, as_completed
from typing import List, Any
import orbit 
from multiprocessing import Pool, cpu_count
from orbit.models import DLT 
import fileUtils

def fit_model(train_data):
    try:
        column_names = ['onpromotion', 'transactions']
        
        model = DLT(response_col='sales', 
                    date_col='date', 
                    seasonality=365,
                    global_trend_option='linear',
                    estimator='stan-map', 
                    damped_factor=0.4, 
                    regressor_col=column_names)
        model.fit(train_data)
        return model
    except Exception as e:
        print(f"Error fitting model: {e}")
        return None

def predict_forecast(model, test_data):
    try:
        forecast = model.predict(test_data)
        return forecast
    except Exception as e:
        print(f"Error predicting forecast: {e}")
        return None

def fit_and_predict(df: pd.DataFrame):
    train_data = df[df['test_indicator'] == 0]
    test_data = df[df['test_indicator'] == 1]
    model = fit_model(train_data)
    if model:
        forecast = predict_forecast(model, test_data)
        return forecast
    else:
        return None

if __name__ == "__main__":
    input_df = fileUtils.read_csv('input_data_item')
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')

    grouped_df = input_df.groupby(['store_item_id', 'date']).agg({'sales':'sum', 'transactions':'sum', 'onpromotion':'sum'}).reset_index()
    grouped_df['test_indicator'] = np.where(grouped_df['date'].dt.year >= 2017, 1, 0)

    # Create batches of data for parallel processing
    num_batches = cpu_count()
    batches = np.array_split(grouped_df, num_batches)

    with ThreadPoolExecutor(max_workers=num_batches) as executor:
        futures = [executor.submit(fit_and_predict, batch) for batch in batches]
        forecast = [future.result() for future in as_completed(futures)]

    forecast_dataframe = pd.concat(forecast)
    
    print(forecast_dataframe.columns)
    print(forecast_dataframe.head(10))
