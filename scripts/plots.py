import matplotlib.pyplot as plt
import pandas as pd
import fileUtils
from statsmodels.tsa.seasonal import seasonal_decompose
import seaborn as sns
import numpy as np

if __name__ == "__main__":

    input_df = fileUtils.read_csv('input_data_item_store')
    print("Shape of dataset: ", input_df.shape)
    print("Number of NA values count", input_df.isna().sum())
    print("Dataset information ", input_df.info())
    print("Dataset analysis ", input_df.describe())
    
    # Convert 'date' column to datetime if it's not already in datetime format
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df.set_index('date', inplace=True)

    # Time series plot
    plt.figure(figsize=(10, 6))
    plt.plot(input_df['sales'])
    plt.title('Sales Over Time')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.grid(True)
    plt.show()


    decomposition = seasonal_decompose(input_df['sales'], model='additive', period=12)
    plt.figure(figsize=(12, 8))

    # Plot original series
    plt.subplot(411)
    plt.plot(input_df['sales'], label='Original Sales')
    plt.legend(loc='upper left')

    # Plot seasonal component
    plt.subplot(412)
    plt.plot(decomposition.seasonal, label='Seasonal Component')
    plt.legend(loc='upper left')

    # Plot trend component
    plt.subplot(413)
    plt.plot(decomposition.trend, label='Trend Component')
    plt.legend(loc='upper left')

    # Plot residual component
    plt.subplot(414)
    plt.plot(decomposition.resid, label='Residual Component')
    plt.legend(loc='upper left')

    plt.tight_layout()
    plt.show()

    # Line Plots comparision between 'sales', 'transactions' & 'onpromotion'
    plt.plot(input_df.index, input_df['sales'], label='Sales')
    plt.plot(input_df.index, input_df['transactions'], label='Transactions')
    plt.plot(input_df.index, input_df['onpromotion'], label='On Promotion')
    plt.title('Sales, Transactions, and On Promotion Over Time')
    plt.xlabel('Date')
    plt.ylabel('Value')
    plt.legend()
    plt.grid(True)
    plt.show()

    # Calculate the correlation matrix
    correlation_matrix = input_df[['sales', 'transactions', 'onpromotion']].corr()

    # Plot the heatmap
    plt.figure(figsize=(8, 6))
    sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
    plt.title('Correlation Heatmap')
    plt.show()

    # Plotting 'sales' against 'onpromotion' with a trend line
    plt.figure(figsize=(8, 6))
    plt.scatter(input_df['onpromotion'], input_df['sales'], alpha=0.5)      
    m, b = np.polyfit(input_df['onpromotion'], input_df['sales'], 1)  
    plt.plot(input_df['onpromotion'], m*input_df['onpromotion'] + b, color='red')  
    plt.title('Sales vs. On Promotion')
    plt.xlabel('On Promotion')
    plt.ylabel('Sales')
    plt.grid(True)
    plt.show()

    # Create subplots for histograms
    fig, axs = plt.subplots(3, 1, figsize=(8, 12))

    # Plot histogram for 'sales'
    axs[0].hist(input_df['sales'], bins=20, color='blue', alpha=0.7)
    axs[0].set_title('Sales Distribution')
    axs[0].set_xlabel('Sales')
    axs[0].set_ylabel('Frequency')

    # Plot histogram for 'transactions'
    axs[1].hist(input_df['transactions'], bins=20, color='green', alpha=0.7)
    axs[1].set_title('Transactions Distribution')
    axs[1].set_xlabel('Transactions')
    axs[1].set_ylabel('Frequency')

    # Plot histogram for 'onpromotion'
    axs[2].hist(input_df['onpromotion'], bins=20, color='orange', alpha=0.7)
    axs[2].set_title('On Promotion Distribution')
    axs[2].set_xlabel('On Promotion')
    axs[2].set_ylabel('Frequency')

    plt.tight_layout()
    plt.show()



