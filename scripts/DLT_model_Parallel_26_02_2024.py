import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from multiprocessing import Pool, cpu_count
from typing import List, Any
from orbit.models import DLT 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt 

def fit_model(train_data, test_data):
    try:
        # train_df = train_data[(train_data['date'].dt.year >= 2013) & (train_data['date'].dt.year <= 2016)]
        # test_df = test_data[test_data['date'].dt.year == 2017]
        column_names = ['onpromotion', 'transactions']
        
        model = DLT(response_col='sales', 
                    date_col='date', 
                    seasonality=365,
                    global_trend_option='linear', 
                    damped_factor=0.4, 
                    regressor_col=column_names)
        model.fit(train_data)
        return model
    except Exception as e:
        print(f"Error fitting model: {e}")
        return None

def predict_forecast(model, test_data):
    try:
        forecast = model.predict(test_data)
        return forecast
    except Exception as e:
        print(f"Error predicting forecast: {e}")
        return None

def fit_and_predict(subset):
    train_data, test_data = subset
    model = fit_model(train_data, test_data)
    if model:
        forecast = predict_forecast(model, test_data)
        return forecast
    else:
        return None

if __name__ == "__main__":
    try:
        input_df = fileUtils.read_csv('input_data')

        scaler = StandardScaler()
        num_cols = ['sales', 'transactions', 'onpromotion']
        input_df[num_cols] = scaler.fit_transform(input_df[num_cols])
        input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
        # # Check for and remove duplicate datetime values
        # input_df = input_df[~input_df.duplicated(subset=['date'], keep='first')]

        # # Sort DataFrame by datetime index
        # input_df.sort_index(inplace=True)

        data_subsets = []

        for store_item_id, group in input_df.groupby('store_item_id'):
            group = group[~group.duplicated(subset=['date'], keep='first')]
            group.sort_index(inplace=True)
            train_data = group[group['date'].dt.year < 2017]
            test_data = group[group['date'].dt.year == 2017]
            data_subsets.append((train_data, test_data))

        with Pool(cpu_count()) as p:
            forecast: List[Any] = list(p.imap(fit_and_predict, data_subsets))

        forecast_dataframe = pd.concat(forecast)
        print(forecast_dataframe.columns)
        print(forecast_dataframe.head(10))
    except Exception as e:
        print(f"Error: {e}")
    
    # # Calculate metrics
    # lr_mse = mean_squared_error(test_data['sales'].astype(float), forecast_dataframe['prediction'])
    # lr_mae = mean_absolute_error(test_data['sales'].astype(float), forecast_dataframe['prediction'])

    # # Apply the absolute value function to both y_eval and lr_predictions
    # y_eval_abs = abs(test_data['sales'].astype(float))
    # lr_predictions_abs = abs(forecast_dataframe['prediction'])

    # # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    # lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # # Create a DataFrame to store results for Linear Regression
    # results_lr = pd.DataFrame({'Model': ['Orbit DLT Model'],
    #                         'RMSLE': [lr_rmsle],
    #                         'RMSE': [np.sqrt(lr_mse)],
    #                         'MSE': [lr_mse],
    #                         'MAE': [lr_mae]}).round(2)

    # # Print the results_lr dataframe
    # print(results_lr)

    # # Plot the actual sales and the forecasted values
    # plt.figure(figsize=(12, 6))
    # plt.plot(test_data['date'], test_data['sales'], label='Actual Sales', color='blue')
    # plt.plot(forecast_dataframe['date'], forecast_dataframe['prediction'], label='Forecasted Sales', color='red')
    # plt.xlabel('Date')
    # plt.ylabel('Sales')
    # plt.title('Actual vs. Forecasted Sales')
    # plt.legend()
    # plt.grid(True)
    # plt.show()


