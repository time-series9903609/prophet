import pandas as pd
import os 
import numpy as np
import fileUtils 
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt 
import orbit
from orbit.models import DLT 
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from joblib import Parallel, delayed

def fit_model(train_data, test_data):

    train_data.drop_duplicates(subset=['date'], keep='first', inplace=True)
    train_df = train_data[(train_data['date'].dt.year >= 2013) & (train_data['date'].dt.year <= 2016)]
    test_df = test_data[test_data['date'].dt.year == 2017]
    column_names = ['onpromotion','transactions']
    
    # Example code to fit DLT model
    model = DLT(response_col='sales', 
                date_col='date', 
                seasonality=365,
                global_trend_option='linear', 
                damped_factor=0.4, 
                regressor_col=column_names)
    # Initialize DLT model with appropriate parameters
    model.fit(train_df)
    
    # Return fitted model
    return model.predict(test_df)

if __name__ == "__main__":

    input_df = fileUtils.read_csv('input_data')

    scaler = StandardScaler()

    # select numerical columns
    num_cols = ['sales', 'transactions', 'onpromotion']
    input_df[num_cols] = scaler.fit_transform(input_df[num_cols])
    print(input_df.head())
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')

    data_subsets = {}

    for store_item in input_df['store_item_id'].unique():

        series_df = input_df[input_df['store_item_id']==store_item]
        train_data = series_df.copy()  # Train data includes all data for the store_item
        test_data = series_df[series_df['date'].dt.year == 2017].copy()  # Test data for 2017
        data_subsets[store_item] = (train_data, test_data)  # Store both train and test data
    
    forecasts = Parallel(n_jobs=-1)(delayed(fit_model)(*subset) for subset in data_subsets.values())

    print(forecasts.columns)
    print(forecasts.head(10))


