import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
# from multiprocessing import Pool, cpu_count
from typing import List, Any
import orbit
from orbit.models import DLT 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime 

# def fit_model(train_data):
#     try:
#         column_names = ['onpromotion', 'transactions']
        
#         model = DLT(response_col='sales', 
#                     date_col='date', 
#                     seasonality=365,
#                     global_trend_option='linear', 
#                     damped_factor=0.4, 
#                     regressor_col=column_names)
#         model.fit(train_data)
#         return model
#     except Exception as e:
#         print(f"Error fitting model: {e}")
#         return None

# def predict_forecast(model, test_data):
#     try:
#         forecast = model.predict(test_data)
#         return forecast
#     except Exception as e:
#         print(f"Error predicting forecast: {e}")
#         return None

# def fit_and_predict(df:pd.DataFrame):
#     train_data = df.loc[df['test_indicator'] == 0]
#     test_data = df.loc[df['test_indicator'] == 1]
#     model = fit_model(train_data)
#     if model:
#         forecast = predict_forecast(model, test_data)
#         return forecast
#     else:
#         return None

if __name__ == "__main__":
    # try:
    input_df = fileUtils.read_csv('input_data_item_store')

    # scaler = StandardScaler()
    # num_cols = ['sales', 'transactions', 'onpromotion']
    # input_df[num_cols] = scaler.fit_transform(input_df[num_cols])
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')

    grouped_df = input_df.groupby(['store_item_id', 'date']).agg({'sales':'sum', 'transactions':'sum', 'onpromotion':'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['date'].dt.year >= 2017, 'test_indicator'] = 1

    train_data = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_data = grouped_df.loc[grouped_df['test_indicator'] == 1]

    column_names = ['onpromotion', 'transactions']
    
    model_time = datetime.now()
    model_timef = model_time.strftime("%Y-%m-%d %H:%M:%S")
    print("Model time:", model_timef)
    model = DLT(response_col='sales', 
                date_col='date', 
                seasonality=365,
                global_trend_option='linear', 
                damped_factor=0.4, 
                regressor_col=column_names)
    model.fit(train_data)
    predict_time = datetime.now()
    predict_timef = predict_time.strftime("%Y-%m-%d %H:%M:%S")
    print("Predict start time:", predict_timef)
    forecast = model.predict(test_data)
    predict_complete_time = datetime.now()
    predict_complete_timef = predict_complete_time.strftime("%Y-%m-%d %H:%M:%S")
    print("Predict complete time:", predict_complete_timef)

    print(forecast.head(20))

    # Calculate metrics
    lr_mse = mean_squared_error(test_data['sales'].astype(float), forecast['prediction'])
    lr_mae = mean_absolute_error(test_data['sales'].astype(float), forecast['prediction'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_data['sales'].astype(float))
    lr_predictions_abs = abs(forecast['prediction'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Orbit Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)],
                            'MSE': [lr_mse],
                            'MAE': [lr_mae]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)

    # plt.plot(test_data.index, test_data.values, label='Actual Data')
    # plt.plot(forecast.index, forecast.values, label='Forecast Data')
    # plt.xlabel('Date')
    # plt.ylabel('Sales')  
    # plt.title('Actual Data vs Forecast Data')
    # plt.legend()
    # plt.show()
