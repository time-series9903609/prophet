import os
import pandas as pd 
import numpy as np 


def read_csv(filename:str):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = filename + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    df = pd.read_csv(file_path)
    return df

def to_dateType(dataframe:pd.DataFrame):

    if 'date' in dataframe.columns:
        dataframe['date'] = pd.to_datetime(dataframe['date'])
    
    return dataframe




